Name:           perl-Sys-MemInfo
Version:        0.99
Release:        10
Summary:        Memory information as Perl module
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Sys-MemInfo
Source0:        https://cpan.metacpan.org/authors/id/S/SC/SCRESTO/Sys-MemInfo-%{version}.tar.gz
BuildRequires:  gcc perl-devel perl-generators perl(ExtUtils::MakeMaker) perl(File::Copy)
BuildRequires:  perl(DynaLoader) perl(Exporter) perl(strict) perl(warnings)
BuildRequires:  perl(Test::More) perl(Data::Dumper)

%{?perl_default_filter}

%description
Sys::MemInfo returns the total amount of free and used physical memory in
bytes in totalmem and freemem variables.

%package_help

%prep
%autosetup -n Sys-MemInfo-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="$RPM_OPT_FLAGS"
%{make_build}

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test TEST_VERBOSE=1

%files
%license LICENSE
%doc Changes README
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/Sys*

%files help
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.99-10
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Dec 04 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.99-9
- Package init
